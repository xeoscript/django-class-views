def param_type(**kwargs):
    def decorator(func):
        for key, value in kwargs.items():
            setattr(func, '__regexp__{0}__'.format(key), value)
        return func

    return decorator