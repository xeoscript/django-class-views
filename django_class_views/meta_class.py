# coding=utf-8
from inspect import getargspec

__author__ = 'ajumell'


class ViewMetaClass(type):
    @classmethod
    def get_pattern(mcs, action, action_name, args, attrs, base, formatted_name, prefix):
        """

        :param action: The actual action function.
        :param action_name: The name of the function.
        :param args: Arguments passed to the function.
        :param attrs: The attributes of the View class.
        :param base: The parents of the view class.
        :param formatted_name: The formatted name of the function.
        :param prefix: The URL prefix of the View class.
        :return:
        """
        arg_len = len(args)
        if base is not None:
            pattern = getattr(base, action_name + '_url_pattern', None)
        else:
            pattern = attrs.get(action_name + '_url_pattern')
        if pattern is None:
            pattern = '^' + prefix + '/'

            if arg_len == 1:
                pattern += formatted_name

            elif arg_len == 2:
                regexp = getattr(action, '__regexp__{0}__'.format(args[1]), '.+')
                pattern += "(?P<{0}>{1})/".format(args[1], regexp)
                pattern += formatted_name

            pattern += "/$"
        return pattern

    @classmethod
    def get_url_name(mcs, action_name, attrs, base, formatted_name, prefix):
        if base is not None:
            url_name = getattr(base, action_name + '_url_name', None)
        else:
            url_name = attrs.get(action_name + '_url_name')

        if url_name is None:
            url_name = prefix + '-' + formatted_name

        return url_name

    @classmethod
    def prepare_action(mcs, action, key, attrs, prefix, base=None):
        if not hasattr(action, '__call__'):
            raise Exception("Invalid action.")

        # Strip action_from the name
        action_name = key[7:]

        formatted_name = action_name
        while "_" in formatted_name:
            formatted_name = formatted_name.replace('_', '-')

        inspection = getargspec(action)
        args = list(inspection.args)

        needs_request = False
        if 'request' in args:
            needs_request = True
            args.remove('request')

        needs_user = False
        if 'user' in args:
            needs_user = True
            args.remove('user')

        pattern = mcs.get_pattern(action, action_name, args, attrs, base, formatted_name, prefix)
        url_name = mcs.get_url_name(action_name, attrs, base, formatted_name, prefix)

        attrs[action_name + '_url_pattern'] = pattern
        attrs[action_name + '_url_name'] = url_name
        attrs[action_name + '_need_user'] = needs_user
        attrs[action_name + '_need_request'] = needs_request
        attrs[action_name + '_args_len'] = len(args)

    @classmethod
    def prepare_actions(mcs, attrs, prefix, bases):
        # Prepare actions in the parent class.
        for base in bases:
            for key in dir(base):
                if key.startswith('action_') and key not in attrs:
                    action = getattr(base, key)
                    mcs.prepare_action(action, key, attrs, prefix, base)

        actions = []
        for key in attrs:
            if key.startswith('action_'):
                actions.append(key)

        for key in actions:
            action = attrs.get(key)
            mcs.prepare_action(action, key, attrs, prefix)

    def __new__(mcs, name, bases, attrs):
        url_prefix = attrs.get('url_prefix')

        if url_prefix:
            mcs.prepare_actions(attrs, url_prefix, bases)

        obj = type.__new__(mcs, name, bases, attrs)
        return obj
