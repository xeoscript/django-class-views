# coding=utf-8

import types

from django.core.urlresolvers import reverse
from django.http import HttpResponseForbidden
from django.shortcuts import redirect, render
from django.utils.decorators import classonlymethod
from django.utils import six

from meta_class import ViewMetaClass


class View(six.with_metaclass(ViewMetaClass, object)):
    url_prefix = None
    staff_only_view = False

    #
    # Class Methods
    #
    def get_action(self, action_name):
        fn = getattr(self, 'action_' + action_name)
        return fn

    @classonlymethod
    def as_view(cls):
        """
        Main entry point for a request-response process.
        """

        def view(request, action, *args, **kwargs):
            self = cls()
            self.request = request
            self.action = action

            self.args = args
            self.kwargs = kwargs

            fn = self.get_action(action)

            if fn is None:
                raise Exception('Action not found : %s' % action)

            perm = self.has_perm_for_action(action, request.user)
            if perm is not True:
                return self.handle_no_permission()

            if getattr(self, action + "_need_request"):
                kwargs['request'] = request

            if getattr(self, action + "_need_user"):
                kwargs['user'] = request.user

            try:
                return fn(*args, **kwargs)
            except Exception, ex:
                print ex
                print action
                print args
                print kwargs
                raise

        return view

    # noinspection PyMethodMayBeStatic,PyBroadException
    def handle_no_permission(self):
        user = self.get_user()
        if user.is_authenticated():
            return self.no_permission_error()
        try:
            return self.redirect_to_login()
        except Exception:
            return self.no_permission_error()

    # noinspection PyMethodMayBeStatic
    def no_permission_error(self):
        return HttpResponseForbidden()

    # noinspection PyMethodMayBeStatic
    def redirect_to_login(self):
        return redirect('login')

    #
    # URL Generation
    #

    @classmethod
    def urls(cls):
        from django.conf.urls import patterns, url

        url_patterns = patterns('')
        props = dir(cls)
        for prop in props:
            if prop.startswith('action_'):
                action_name = prop.replace('action_', '')
                result = cls.get_url_for_action(action_name)
                if result is not None:
                    pattern, url_name = result
                    url_patterns.append(url(pattern, cls.as_view(), {
                        'action': action_name
                    }, url_name))

        return url_patterns

    @classmethod
    def get_url_name_for_action(cls, action_name):
        return cls.get_url_for_action(action_name)[1]

    @classmethod
    def has_action(cls, action_name):
        if action_name:
            return hasattr(cls, "action_" + action_name)

    @classmethod
    def get_url_for_action(cls, action_name):
        pattern = getattr(cls, action_name + "_url_pattern")
        url_name = getattr(cls, action_name + "_url_name")
        return pattern, url_name

    @classmethod
    def redirect_to_action(cls, action_name, args=None, kwargs=None):
        return redirect(cls.link_to_action(action_name, args, kwargs))

    @classmethod
    def link_to_action(cls, action_name, args=None, kwargs=None):
        url_name = cls.get_url_name_for_action(action_name)
        return reverse(url_name, args=args, kwargs=kwargs)

    @classmethod
    def get_action_args_len(cls, action_name):
        return getattr(cls, action_name + "_args_len", 1)

    #
    # Permissions
    #

    def get_permission(self, action=None):
        if action is None:
            action = getattr(self, 'action', None)

        perm = getattr(self, action + "_permission", None)
        if perm is None:
            return None

        return perm

    def has_perm_for_action(self, action, user):
        perm = self.get_permission(action)
        if not perm:
            return True

        can_check = user.is_active
        if self.staff_only_view:
            can_check = can_check and (user.is_staff or user.is_superuser)

        return can_check and user.has_perm(perm)

    #
    # Getters
    #

    def _get_attr(self, name, *args, **kwargs):

        obj = getattr(self, name, None)
        if obj is None:
            default = kwargs.pop('default', None)
            return default

        if isinstance(obj, (types.FunctionType, types.MethodType)):
            try:
                obj = obj(*args, **kwargs)
            except Exception:
                raise Exception('%s should receive *args and **kwargs' % name)

        return obj

    def _get_action_attr(self, name, *args, **kwargs):
        action = kwargs.pop('action', None)
        if action is None:
            action = getattr(self, 'action')
        name = action + "_" + name
        return self._get_attr(name, *args, **kwargs)


    #
    # Helpers
    #

    def render(self, template, kwargs=None):
        request = getattr(self, 'request')
        if kwargs is None:
            kwargs = {}

        return render(request, template, kwargs)

    def get_request(self):
        return getattr(self, 'request', None)

    def get_user(self):
        request = self.get_request()
        user = request.user
        return user